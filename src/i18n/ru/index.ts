export default {
  extensions: {
    Blockquote: {
      buttons: {
        blockquote: {
          tooltip: 'Блок цитаты'
        }
      }
    },
    Bold: {
      buttons: {
        bold: {
          tooltip: 'Жирный'
        }
      }
    },
    BulletList: {
      buttons: {
        bulletList: {
          tooltip: 'Маркированный список'
        }
      }
    },
    Code: {
      buttons: {
        code: {
          tooltip: 'Код'
        }
      }
    },
    CodeBlock: {
      buttons: {
        codeBlock: {
          tooltip: 'Блок кода'
        }
      }
    },
    History: {
      buttons: {
        undo: {
          tooltip: 'Назад'
        },
        redo: {
          tooltip: 'Вперед'
        }
      }
    },
    HorizontalRule: {
      buttons: {
        horizontalRule: {
          tooltip: 'Горизонтальная линия'
        }
      }
    },
    Italic: {
      buttons: {
        italic: {
          tooltip: 'Курсивный'
        }
      }
    },
    Notice: {
      buttons: {
        notice: {
          tooltip: 'Важно'
        }
      }
    },
    OrderedList: {
      buttons: {
        orderedList: {
          tooltip: 'Упорядоченный список'
        }
      }
    },
    Paragraph: {
      buttons: {
        paragraph: {
          tooltip: 'Параграф'
        }
      }
    },
    Strike: {
      buttons: {
        strike: {
          tooltip: 'Перечерктнутый'
        }
      }
    },
    Underline: {
      buttons: {
        underline: {
          tooltip: 'Подчерктнутый'
        }
      }
    },
    Heading: {
      buttons: {
        heading: {
          tooltip: ({ level }) => `Заголовок ${level} уровня`
        }
      }
    },
    Link: {
      buttons: {
        isActive: {
          tooltip: 'Изменить ссылку'
        },
        notActive: {
          tooltip: 'Добавить ссылку'
        }
      },
      window: {
        title: 'Управление ссылкой',
        form: {
          hrefLabel: 'Href'
        },
        buttons: {
          close: 'Закрыть',
          remove: 'Удалить',
          apply: 'Применить'
        }
      }
    },
    Image: {
      buttons: {
        tooltip: 'Картинка'
      },
      window: {
        title: 'Добавить картинку',
        form: {
          sourceLink: 'Ссылка на картинку',
          altText: 'Подпись',
          addImage: 'Добавить картинку'
        },
        imageUpload: {
          instruction: 'Выберите файл (ы) или перетащите его (их) сюда.'
        },
        buttons: {
          close: 'Закрыть',
          apply: 'Применить'
        }
      }
    },
    Iframe: {
      buttons: {
        tooltip: 'Видео'
      },
      window: {
        title: 'Добавить видео',
        form: {
          videoID: 'ID ютуб видео',
          width: 'ширина видео (px, %)',
          height: 'высота видео (px, %)',
        },
        buttons: {
          close: 'Закрыть',
          apply: 'Применить'
        }
      }
    },
    TodoList: {
      buttons: {
        todoList: {
          tooltip: 'Список дел'
        }
      }
    }
  }
}
