import { Node } from 'tiptap'
import { nodeInputRule } from 'tiptap-commands'

const IFRAME_INPUT_REGEX = /!\[(.+|:?)]\((\S+)(?:(?:\s+)["'](\S+)["'])?\)/

export default class IframeNode extends Node {

  get name() {
    return 'iframe'
  }

  get schema() {
    return {
      attrs: {
        src: {
          default: null,
        },
        width: {
          default: '100%'
        },
        height: {
          default: '100%'
        }
      },
      group: 'block',
      selectable: false,
      parseDOM: [{
        tag: 'iframe[src]',
        getAttrs: dom => ({
          src: dom.getAttribute('src'),
          width: dom.getAttribute('width'),
          height: dom.getAttribute('height')
        }),
      }],
      toDOM: node => ['iframe', {
        src: node.attrs.src,
        width: node.attrs.width,
        height: node.attrs.height,
        frameborder: 0,
        allowfullscreen: 'true',
      }],
    }
  }

  commands({ type }) {
    return attrs => (state, dispatch) => {
      const { selection } = state
      const position = selection.$cursor ? selection.$cursor.pos : selection.$to.pos
      const node = type.create(attrs)
      const transaction = state.tr.insert(position, node)
      dispatch(transaction)
    }
  }

  inputRules({ type }) {
    return [
      nodeInputRule(IFRAME_INPUT_REGEX, type, match => {
        const [, src] = match
        return {
          src,
        }
      }),
    ]
  }
}