import IframeNode from '~/extensions/nativeExtensions/iframe/IframeNode.js'
import { VuetifyIconsGroups } from '~/configs/theme'
import VuetifyIcon from '~/extensions/nativeExtensions/icons/VuetifyIcon'
import I18nText from '~/i18n/I18nText'
import AbstractExtension from '~/extensions/AbstractExtension'
import ExtensionActionInterface from '~/extensions/actions/ExtensionActionInterface'
import Vue from 'vue'
import ExtensionActionRenderBtn from '~/extensions/actions/renders/btn/ExtensionActionRenderBtn.ts'
import IframeWindow from '~/extensions/nativeExtensions/iframe/IframeWindow.vue'

export default class Iframe extends AbstractExtension {
  constructor (options) {
    super(options, IframeNode)
  }

  get availableActions (): ExtensionActionInterface[] {
    const nativeExtensionName = 'iframe'

    return [
      {
        render: new ExtensionActionRenderBtn({
          tooltip: new I18nText('extensions.Iframe.buttons.tooltip'),
          icons: {
            [VuetifyIconsGroups.md]: new VuetifyIcon('video'),
            [VuetifyIconsGroups.fa]: new VuetifyIcon('fas fa-video'),
            [VuetifyIconsGroups.mdi]: new VuetifyIcon('mdi-video'),
            [VuetifyIconsGroups.mdiSvg]: new VuetifyIcon('M8.5,13.5L11,16.5L14.5,12L19,18H5M21,19V5C21,3.89 20.1,3 19,3H5A2,2 0 0,0 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19Z')
          },
          nativeExtensionName,
          async onClick ({ context, editor }) {
            const LinkWindowComponent = Vue.extend(IframeWindow)
            const instance = new LinkWindowComponent({
              vuetify: Vue.prototype.tiptapVuetifyPlugin.vuetify,
              propsData: {
                value: true,
                nativeExtensionName,
                context,
                editor,
              }
            })

            instance.$mount()
            document.querySelector('body')!.appendChild(instance.$el)
            
          }
        })
      }
    ]
  }
}
