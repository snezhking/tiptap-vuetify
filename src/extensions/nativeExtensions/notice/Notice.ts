import NoticeMark from '~/extensions/nativeExtensions/notice/NoticeMark'
import { VuetifyIconsGroups } from '~/configs/theme'
import VuetifyIcon from '~/extensions/nativeExtensions/icons/VuetifyIcon'
import I18nText from '~/i18n/I18nText'
import ExtensionActionInterface from '~/extensions/actions/ExtensionActionInterface'
import ExtensionActionRenderBtn from '~/extensions/actions/renders/btn/ExtensionActionRenderBtn.ts'
import AbstractExtension from '~/extensions/AbstractExtension'

export default class Notice extends AbstractExtension {
  constructor (options) {
    super(options, NoticeMark)
  }

  get availableActions (): ExtensionActionInterface[] {
    return [
      {
        render: new ExtensionActionRenderBtn({
          tooltip: new I18nText('extensions.Notice.buttons.notice.tooltip'),
          icons: {
            [VuetifyIconsGroups.md]: new VuetifyIcon('info'),
            [VuetifyIconsGroups.fa]: new VuetifyIcon('fa-exclamation-circle'),
            [VuetifyIconsGroups.mdi]: new VuetifyIcon('mdi-alert-circle'),
            [VuetifyIconsGroups.mdiSvg]: new VuetifyIcon('M10,4V7H12.21L8.79,15H6V18H14V15H11.79L15.21,7H18V4H10Z')
          },
          nativeExtensionName: 'notice'
        })
      }
    ]
  }
}
