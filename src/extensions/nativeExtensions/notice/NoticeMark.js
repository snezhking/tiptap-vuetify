import { Mark } from 'tiptap'
import { toggleMark } from 'tiptap-commands'

export default class NoticeMark extends Mark {

  get name() {
    return 'notice'
  }

  get schema() {
    return {
      parseDOM: [
        { tag: 'div', class: 'notice' },
      ],
      toDOM: node => ['div', { class: 'notice' }],
    }
  }

  commands({ type }) {
    return () => toggleMark(type)
  }
}
