import AbstractExtension from '~/extensions/AbstractExtension'
import ExtensionActionInterface from '~/extensions/actions/ExtensionActionInterface'
import FigcaptionNode from '~/extensions/nativeExtensions/image/FigcaptionNode'

export default class Figcaption extends AbstractExtension {
  constructor (options) {
    super(options, FigcaptionNode)
  }

  get availableActions (): ExtensionActionInterface[] {
    return []
  }
}
