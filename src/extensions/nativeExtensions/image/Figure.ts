import AbstractExtension from '~/extensions/AbstractExtension'
import ExtensionActionInterface from '~/extensions/actions/ExtensionActionInterface'
import FigureNode from '~/extensions/nativeExtensions/image/FigureNode'

export default class Figure extends AbstractExtension {
  constructor (options) {
    super(options, FigureNode)
  }

  get availableActions (): ExtensionActionInterface[] {
    return []
  }
}
