import {Node} from 'tiptap'

export default class FigcaptionNode extends Node {

  get name() {
    return "figcaption"
  }

  get schema() {
    return {
      content: "inline*",
      group: "figure",
      parseDOM: [{tag: "figcaption"}],
      toDOM: node => ["figcaption", 0],
    }
  }
}