import { Node   } from 'tiptap'
import { Plugin } from 'prosemirror-state'

export default class FigureNode extends Node {

  get name() {
    return "figure";
  }

  get schema() {
    return {
      attrs: {class: {default: null}},
      content: "image figcaption",
      group: "block",
      parseDOM: [{
        tag: "figure",
        getAttrs: dom => ({
          class: dom.getAttribute('class')
        })
      }],
      toDOM: node =>  ["figure", node.attrs, 0],
    };
  }

  commands({ type, schema }) {
    return attrs => (state, dispatch) => {
      const { tr, selection } = state
      const position = selection.$cursor ? selection.$cursor.pos : selection.$to.pos
      const child = [
        schema.nodes.image.create({
          alt: attrs.alt,
          src: attrs.src,
        }),
      ]
      if (attrs.caption) child.push(schema.nodes.figcaption.create(
          null,
          schema.text(attrs.caption)
        ),)
      const node = type.create({class: attrs.class}, child)
      dispatch(tr.insert(position, node))
    }
  }

  // Deletes the entire figure node if imageNode is empty.
  get plugins() {
    return [
      new Plugin ({
        appendTransaction: (transactions, oldState, newState) => {
          const tr = newState.tr
          let modified = false;
          // TO-DO: Iterate through transactions instead of descendants.
          newState.doc.descendants((node, pos, parent) => {
            if (node.type.name != "figure") return;
            let imageNode = node.firstChild;
            if (imageNode.attrs.src == imageNode.type.defaultAttrs.src &&
                imageNode.attrs.alt == imageNode.type.defaultAttrs.alt)
              {
                tr.deleteRange(pos, pos + node.nodeSize);
                modified = true;
              }
          })
          if (modified) return tr;
        }
      })
    ];
  }

}