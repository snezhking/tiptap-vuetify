import { Node, Plugin } from "tiptap"

export default class Image extends Node {

  get name() {
    return "image"
  }

  get schema() {
    return {
      inline: false,
      attrs: {src: {default: ""}, alt: {default: ""}},
      parseDOM: [{tag: "img", getAttrs: dom => ({src: dom.src, alt: dom.alt})}],
      toDOM: node => ["img", node.attrs],
    }
  }

  commands({ type }) {
    return attrs => (state, dispatch) => {
      const { selection } = state
      const position = selection.$cursor ? selection.$cursor.pos : selection.$to.pos
      const node = type.create(attrs)
      const transaction = state.tr.insert(position, node)
      dispatch(transaction)
    }
  }

  get plugins() {
    return [
      new Plugin({
        props: {
          handleDOMEvents: {
            drop(view, event) {

              if (!event.dataTransfer || !event.dataTransfer.files || !event.dataTransfer.files.length)
                return

              const images = Array.from(event.dataTransfer.files).filter(file => {
                return (/image/i).test(file.type)
              })

              if (images.length == 0) return

              event.preventDefault()

              const { schema } = view.state
              const coordinates = view.posAtCoords({ left: event.clientX, top: event.clientY })

              images.forEach(image => {

                // This is where you would upload image to a server or a filesystem,
                // then create an imageNode.
                const node = schema.nodes.image.create({src: XXX, alt: YYY})
                const tr = view.state.tr.insert(coordinates.pos, node)
                view.dispatch(tr)

              })

            },
          },
        },
      }),
    ]
  }

}